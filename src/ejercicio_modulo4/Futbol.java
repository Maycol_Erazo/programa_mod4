/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio_modulo4;

import helpers.Futbolista;

/**
 *
 * @author mike
 */
public class Futbol {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //instaciacion de clase
        Futbolista  enlace= new Futbolista("Mike Erazo", "Real Madrid");
        enlace.imprimirNombre();
        enlace.imprimirEquipo();
        //valor de variable privada
        enlace.establecerEdad(19);
        System.out.println(enlace.obtenerEdad());
    }
               
}